# Controller.js

Controller.js creates a simplified way to transport Data View Models from an ASP.NET MVC Controller to a JavaScript controller.

TypeScript is used in this example, but this can just as easily be used with only JavaScript.  JavaScript is included in the sample project.

_note: This is not a "JavaScript MVC framework"_

## Features

* Easily transport Data View Models from the server to client.  Data View Models can be classes, anonymous or dynamic.
* You can pass complex types to the client, like DateTime or Arrays.
* There's no need to worry about the document being ready.  When Controller.js Actions are called, the document is _ready_.
* JavaScript can be executed globally in the ControllerJs.Application class.
* Client side JavaScript is now organized the same way your Server side controllers are, with matching Controllers and Actions.
* No more writing `$(function() { ... })` on every page.
* Use TypeScript or plain old JavaScript.
* Use ASP.NET MVC's bundling to combine and minimize all Controller.js files into a single JavaScript file.

## Usage

Set `ViewBag.ControllerJsData` with the data you want to send to the client side controller.

**Sampe HomeController.cs:**

	using System;
	using System.Web.Mvc;
	
	namespace MyApplication.Controllers
	{
		public class HomeController : Controller
		{
			public ActionResult Index()
			{
				ViewBag.ControllerJsData = new {
					Status = "Connected",
					Date = DateTime.Now,
				};
				
				return View();
			}
		}
	}

Controller.js will call the client side controller with the same name and pass the data into the matching method.

In this example the controller is `HomeController` and the action is `Index`.

**Sample HomeController.ts:**

	module ControllerJs.Controllers
	{
		export class HomeController implements IController
		{
			public Index(data: any)
			{
				$('#controllerJsStatus').text(data.Status);
				$('#controllerJsDate').text(data.Date);
			}
		}
	}

JavaScript you want run on every page can be inserted into the `Application.Application_Start` event.

** Sample Application.ts:**

	module ControllerJs
	{
		export class Application
		{
			public static ViewContext: ViewContext = null;
			public static ControllerResolver: ControllerResolver = null;
	
			public static Application_Init(viewContext: ViewContext): void
			{
				Application.ViewContext = viewContext;
				Application.ControllerResolver = new ControllerResolver(ControllerJs['Controllers']);
	
				Application.Application_Start();
			}
	
			public static Application_Start(): void
			{
				Application.ControllerResolver.TryInvoke(Application.ViewContext);
			}
		}
	}

## Installation

Get the latest ControllerJs package from nuget.

    PM> Install-Package ControllerJs

Add the Bundle to your BundleConfig.cs

    bundles.Add(new ScriptBundle("~/bundles/controllerjs")
        .IncludeDirectory("~/Scripts/ControllerJs/Core", "*.js", false)
        .IncludeDirectory("~/Scripts/ControllerJs", "*.js", true));

Add the Partial View to the bottom of your _Layout.cshtml

    @Html.Partial("_ControllerJs")

