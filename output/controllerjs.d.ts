declare module ControllerJs
{
    function Version(): string;

    interface IController
    {
    }

    class ControllerResolver
    {
        private _controllers: any;
        constructor(controllers: any);
        public GetController(controller: string): IController
        public TryInvoke(viewContext: ViewContext): boolean;
    }

    class ViewContext
    {
        public Controller: string;
        public Action: string;
        public Data: any;
        constructor(Controller: string, Action: string);
        constructor(Controller: string, Action: string, Data: any);
    }
}