/// <reference path="Validate.ts" />
var ControllerJs;
(function (ControllerJs) {
    /**
    * The model that is used by Controller.js
    */
    var ViewContext = (function () {
        function ViewContext(Controller, Action, Data) {
            if (typeof Data === "undefined") { Data = null; }
            this.Controller = Controller;
            this.Action = Action;
            this.Data = Data;
            ControllerJs.Validate.IsNotNullOrEmpty(Controller, "Controller");
            ControllerJs.Validate.IsNotNullOrEmpty(Action, "Action");
        }
        return ViewContext;
    })();
    ControllerJs.ViewContext = ViewContext;
})(ControllerJs || (ControllerJs = {}));
