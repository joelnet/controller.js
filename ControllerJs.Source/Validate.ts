module ControllerJs
{
    /**
     * Parameter Validation Class
     */
    export class Validate
    {
        /**
         * Verifies that the specified object is not null.  An exception is thrown if it is null.
         */
        static IsNotNull(value: any, name: string): boolean
        {
            var isValid: boolean = typeof value !== 'undefined' && value !== null;

            if (!isValid)
            {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        }

        /**
         * Verifies that the specified object is not null or an empty string.  An exception is thrown if it is null or an empty string.
         */
        static IsNotNullOrEmpty(value: any, name: string): boolean
        {
            var isValid: boolean = Validate.IsNotNull(value, name) && value !== '';

            if (!isValid)
            {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        }
    }
}