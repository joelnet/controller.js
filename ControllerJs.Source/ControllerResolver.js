/// <reference path="Controller.ts" />
/// <reference path="ViewContext.ts" />
/// <reference path="Validate.ts" />
var ControllerJs;
(function (ControllerJs) {
    /**
    * Service that resolves the controller and executes it's actions.
    */
    var ControllerResolver = (function () {
        function ControllerResolver(controllers) {
            this._controllers = controllers || {};
        }
<<<<<<< HEAD
=======
        /**
        * Returns true if the controller exists.
        */
        ControllerResolver.prototype.HasController = function (controller) {
            ControllerJs.Validate.IsNotNullOrEmpty(controller, "controller");

            var c = this._controllers[controller + 'Controller'];

            return typeof c !== 'undefined' && c !== null;
        };

        /**
        * Resolves and returns the controller.  If it does not exist null will be returned.
        */
>>>>>>> e9de989a3b1ce45bf7b923eb3cab793fb6b886a4
        ControllerResolver.prototype.GetController = function (controller) {
            ControllerJs.Validate.IsNotNullOrEmpty(controller, 'controller');

            var name = controller.toUpperCase() + 'CONTROLLER';

            for (var key in this._controllers) {
                if (key.toString().toUpperCase() === name) {
                    return new this._controllers[key]();
                }
            }

            return null;
        };

        /**
        * Tries to invoke the action on the controller.  Will return true if successful.
        */
        ControllerResolver.prototype.TryInvoke = function (viewContext) {
            ControllerJs.Validate.IsNotNull(viewContext, "viewContext");
            ControllerJs.Validate.IsNotNull(viewContext.Action, "viewContext.Action");
            ControllerJs.Validate.IsNotNull(viewContext.Controller, "viewContext.Controller");

            var controller = this.GetController(viewContext.Controller);

            if (typeof controller !== 'undefined' && controller !== null) {
                var fn = controller[viewContext.Action];

                if (typeof fn == 'function') {
                    fn.apply(controller, [viewContext.Data]);
                    return true;
                }
            }

            return false;
        };
        return ControllerResolver;
    })();
    ControllerJs.ControllerResolver = ControllerResolver;
})(ControllerJs || (ControllerJs = {}));
