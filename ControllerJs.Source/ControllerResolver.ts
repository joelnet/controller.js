/// <reference path="Controller.ts" />
/// <reference path="ViewContext.ts" />
/// <reference path="Validate.ts" />

module ControllerJs
{
    /**
     * Service that resolves the controller and executes it's actions.
     */
    export class ControllerResolver
    {
        private _controllers: any;

        constructor(controllers: any)
        {
            this._controllers = controllers || {};
        }

        /**
         * Resolves and returns the controller.  If it does not exist null will be returned.
         */
        public GetController(controller: string): IController
        {
            Validate.IsNotNullOrEmpty(controller, 'controller');

            var name: string = controller.toUpperCase() + 'CONTROLLER';

            for (var key in this._controllers)
            {
                if (key.toString().toUpperCase() === name)
                {
                    return new this._controllers[key]();
                }
            }

            return null;
        }

        /**
         * Tries to invoke the action on the controller.  Will return true if successful.
         */
        public TryInvoke(viewContext: ViewContext): boolean
        {
            Validate.IsNotNull(viewContext, "viewContext");
            Validate.IsNotNull(viewContext.Action, "viewContext.Action");
            Validate.IsNotNull(viewContext.Controller, "viewContext.Controller");

            var controller = this.GetController(viewContext.Controller);

            if (controller !== null)
            {
                var fn = this.GetAction(controller, viewContext.Action);

                if (fn !== null)
                {
                    fn.apply(controller, [viewContext.Data]);
                    return true;
                }
            }

            return false;
        }

        /**
         * GetAction Gets the action function in the controller.
         */
        private GetAction(controller: IController, action: string): Function
        {
            for (var key in controller)
            {
                if (action.toUpperCase() === key.toString().toUpperCase()
                    && typeof controller[key] === 'function')
                {
                    return controller[key];
                }
            }

            return null;
        }
    }
}
