var ControllerJs;
(function (ControllerJs) {
    /**
    * Parameter Validation Class
    */
    var Validate = (function () {
        function Validate() {
        }
        Validate.IsNotNull = /**
        * Verifies that the specified object is not null.  An exception is thrown if it is null.
        */
        function (value, name) {
            var isValid = typeof value !== 'undefined' && value !== null;

            if (!isValid) {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        };

        Validate.IsNotNullOrEmpty = /**
        * Verifies that the specified object is not null or an empty string.  An exception is thrown if it is null or an empty string.
        */
        function (value, name) {
            var isValid = Validate.IsNotNull(value, name) && value !== '';

            if (!isValid) {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        };
        return Validate;
    })();
    ControllerJs.Validate = Validate;
})(ControllerJs || (ControllerJs = {}));
