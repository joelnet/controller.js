/// <reference path="Validate.ts" />

module ControllerJs
{
    /**
     * The model that is used by Controller.js
     */
    export class ViewContext
    {
        constructor(public Controller: string, public Action: string, public Data: any = null)
        {
            Validate.IsNotNullOrEmpty(Controller, "Controller");
            Validate.IsNotNullOrEmpty(Action, "Action");
        }
    }
}
