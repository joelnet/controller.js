var ControllerJs;
(function (ControllerJs) {
    /**
    * Parameter Validation Class
    */
    var Validate = (function () {
        function Validate() {
        }
        /**
        * Verifies that the specified object is not null.  An exception is thrown if it is null.
        */
        Validate.IsNotNull = function (value, name) {
            var isValid = typeof value !== 'undefined' && value !== null;

            if (!isValid) {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        };

        /**
        * Verifies that the specified object is not null or an empty string.  An exception is thrown if it is null or an empty string.
        */
        Validate.IsNotNullOrEmpty = function (value, name) {
            var isValid = Validate.IsNotNull(value, name) && value !== '';

            if (!isValid) {
                throw "Argument '" + name + "' cannot be null or empty.";
            }

            return isValid;
        };
        return Validate;
    })();
    ControllerJs.Validate = Validate;
})(ControllerJs || (ControllerJs = {}));
/// <reference path="Validate.ts" />
var ControllerJs;
(function (ControllerJs) {
    /**
    * The model that is used by Controller.js
    */
    var ViewContext = (function () {
        function ViewContext(Controller, Action, Data) {
            if (typeof Data === "undefined") { Data = null; }
            this.Controller = Controller;
            this.Action = Action;
            this.Data = Data;
            ControllerJs.Validate.IsNotNullOrEmpty(Controller, "Controller");
            ControllerJs.Validate.IsNotNullOrEmpty(Action, "Action");
        }
        return ViewContext;
    })();
    ControllerJs.ViewContext = ViewContext;
})(ControllerJs || (ControllerJs = {}));
/// <reference path="ViewContext.ts" />
/// <reference path="Controller.ts" />
/// <reference path="ViewContext.ts" />
/// <reference path="Validate.ts" />
var ControllerJs;
(function (ControllerJs) {
    /**
    * Service that resolves the controller and executes it's actions.
    */
    var ControllerResolver = (function () {
        function ControllerResolver(controllers) {
            this._controllers = controllers || {};
        }
        /**
        * Resolves and returns the controller.  If it does not exist null will be returned.
        */
        ControllerResolver.prototype.GetController = function (controller) {
            ControllerJs.Validate.IsNotNullOrEmpty(controller, 'controller');

            var name = controller.toUpperCase() + 'CONTROLLER';

            for (var key in this._controllers) {
                if (key.toString().toUpperCase() === name) {
                    return new this._controllers[key]();
                }
            }

            return null;
        };

        /**
        * Tries to invoke the action on the controller.  Will return true if successful.
        */
        ControllerResolver.prototype.TryInvoke = function (viewContext) {
            ControllerJs.Validate.IsNotNull(viewContext, "viewContext");
            ControllerJs.Validate.IsNotNull(viewContext.Action, "viewContext.Action");
            ControllerJs.Validate.IsNotNull(viewContext.Controller, "viewContext.Controller");

            var controller = this.GetController(viewContext.Controller);

            if (controller !== null) {
                var fn = this.GetAction(controller, viewContext.Action);

                if (fn !== null) {
                    fn.apply(controller, [viewContext.Data]);
                    return true;
                }
            }

            return false;
        };

        /**
        * GetAction Gets the action function in the controller.
        */
        ControllerResolver.prototype.GetAction = function (controller, action) {
            for (var key in controller) {
                if (action.toUpperCase() === key.toString().toUpperCase() && typeof controller[key] === 'function') {
                    return controller[key];
                }
            }

            return null;
        };
        return ControllerResolver;
    })();
    ControllerJs.ControllerResolver = ControllerResolver;
})(ControllerJs || (ControllerJs = {}));
var ControllerJs;
(function (ControllerJs) {
    /**
    * Current version of Controller.Js
    */
    ControllerJs.Version = "1.0.4";
})(ControllerJs || (ControllerJs = {}));
