module ControllerJs.Models
{
    export class ConnectionViewModel
    {
        constructor(public Status: string, public Date: Date)
        {
        }
    }
}
