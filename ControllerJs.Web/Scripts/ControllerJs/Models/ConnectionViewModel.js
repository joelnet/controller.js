var ControllerJs;
(function (ControllerJs) {
    (function (Models) {
        var ConnectionViewModel = (function () {
            function ConnectionViewModel(Status, Date) {
                this.Status = Status;
                this.Date = Date;
            }
            return ConnectionViewModel;
        })();
        Models.ConnectionViewModel = ConnectionViewModel;
    })(ControllerJs.Models || (ControllerJs.Models = {}));
    var Models = ControllerJs.Models;
})(ControllerJs || (ControllerJs = {}));
//# sourceMappingURL=ConnectionViewModel.js.map
