/// <reference path="../../typings/controllerjs/controllerjs.d.ts" />
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="../Models/ConnectionViewModel.ts" />

module ControllerJs.Controllers
{
    export class HomeController implements IController
    {
        public Index(model: Models.ConnectionViewModel)
        {
            $('#controllerJsStatus').text(model.Status);
            $('#controllerJsDate').text(model.Date);
        }
    }
}
