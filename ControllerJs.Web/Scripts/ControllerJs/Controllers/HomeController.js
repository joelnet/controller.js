var ControllerJs;
(function (ControllerJs) {
    /// <reference path="../../typings/controllerjs/controllerjs.d.ts" />
    /// <reference path="../../typings/jquery/jquery.d.ts" />
    /// <reference path="../Models/ConnectionViewModel.ts" />
    (function (Controllers) {
        var HomeController = (function () {
            function HomeController() {
            }
            HomeController.prototype.Index = function (model) {
                $('#controllerJsStatus').text(model.Status);
                $('#controllerJsDate').text(model.Date);
            };
            return HomeController;
        })();
        Controllers.HomeController = HomeController;
    })(ControllerJs.Controllers || (ControllerJs.Controllers = {}));
    var Controllers = ControllerJs.Controllers;
})(ControllerJs || (ControllerJs = {}));
