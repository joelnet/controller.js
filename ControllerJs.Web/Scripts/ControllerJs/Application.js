/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/controllerjs/controllerjs.d.ts" />
var ControllerJs;
(function (ControllerJs) {
    var Application = (function () {
        function Application() {
        }
        Application.Application_Init = function (viewContext) {
            Application.ViewContext = viewContext;
            Application.ControllerResolver = new ControllerJs.ControllerResolver(ControllerJs['Controllers']);

            $(function () {
                Application.Application_Start();
            });
        };

        Application.Application_Start = function () {
            Application.ControllerResolver.TryInvoke(Application.ViewContext);
        };
        Application.ViewContext = null;
        Application.ControllerResolver = null;
        return Application;
    })();
    ControllerJs.Application = Application;
})(ControllerJs || (ControllerJs = {}));
//# sourceMappingURL=Application.js.map
