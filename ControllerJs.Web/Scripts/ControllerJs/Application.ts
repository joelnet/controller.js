/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/controllerjs/controllerjs.d.ts" />

module ControllerJs
{
    export class Application
    {
        public static ViewContext: ViewContext = null;
        public static ControllerResolver: ControllerResolver = null;

        public static Application_Init(viewContext: ViewContext): void
        {
            Application.ViewContext = viewContext;
            Application.ControllerResolver = new ControllerResolver(ControllerJs['Controllers']);

            $(function () {
                Application.Application_Start();
            });
        }

        public static Application_Start(): void
        {
            Application.ControllerResolver.TryInvoke(Application.ViewContext);
        }
    }
}
