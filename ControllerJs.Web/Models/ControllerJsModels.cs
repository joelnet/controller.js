﻿using System;

namespace ControllerJs.Models
{
    public class ConnectionViewModel
    {
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
}