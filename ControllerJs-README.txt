Add the Bundle to your BundleConfig.cs

    bundles.Add(new ScriptBundle("~/bundles/controllerjs")
        .IncludeDirectory("~/Scripts/ControllerJs/Core", "*.js", false)
        .IncludeDirectory("~/Scripts/ControllerJs", "*.js", true));

Add the Partial View to the bottom of your _Layout.cshtml

    @Html.Partial("_ControllerJs")

